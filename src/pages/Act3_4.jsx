import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Act3_4() {
    const imageUrls = [
        "https://media2.giphy.com/media/rw24BAriH3mR56vrL7/giphy.webp?cid=790b7611cwhvi44rech8xngwz8slcn1o5d936c002j8jeyen&ep=v1_gifs_search&rid=giphy.webp&ct=g",
        "https://media0.giphy.com/media/v1.Y2lkPTc5MGI3NjExb2RsaWZtbmNsNDhpd2thOWM5YXB5OHFkMTdyaDEyMW1hd25qMnVjaiZlcD12MV9naWZzX3NlYXJjaCZjdD1n/xUA7b1AZjL2fQNUp8s/200.webp",
        "https://media0.giphy.com/media/jKQJzaN1R6qC9EouoJ/200.webp?cid=790b7611xaqfgjg79zwb4142y3k3iw63xu8se4abuw6bb5kp&ep=v1_gifs_search&rid=200.webp&ct=g",
        "https://media2.giphy.com/media/v1.Y2lkPTc5MGI3NjExZHB3ZnB3bWFkNWJraXUzd2Fub2F0dXk0MzVhYzl3ZG96djVuZWtmZyZlcD12MV9naWZzX3NlYXJjaCZjdD1n/tk9vT7pPVNyLmtfRYC/giphy.webp",
        "https://media2.giphy.com/media/v1.Y2lkPTc5MGI3NjExMXF5azhmb2NmbWttcGFmbzN1OGFiYTJ3NGN6dzBudGNydGRycWVnMCZlcD12MV9naWZzX3NlYXJjaCZjdD1n/vlK6BBJUE5lq3O4r2n/giphy.webp",
        "https://media4.giphy.com/media/v1.Y2lkPTc5MGI3NjExaGV0dmtxNmgwa3U4bGJraWxnNHNldnd6bnlraGEwejB5ZzdsNmVmbyZlcD12MV9naWZzX3NlYXJjaCZjdD1n/3o7TKJkUIgrWrJdOWA/200.webp",
        "https://media2.giphy.com/media/3otPozEs14AOGrdcOI/200.webp?cid=790b7611xmzzpknqczhdqqtnsshwpjsktitx64f6daeqtebq&ep=v1_gifs_search&rid=200.webp&ct=g"
      ];

    const CardTitles = [
        "Alchemy of Souls",
        "Natures Girl",
        "My Secretary Kim",
        "Workout Life",
        "My secret romance",
        "Merlin",
        "Gilmore Girls"
    ];

    const CardTexts = [
        "In the alchemy of souls, hearts intertwine, Each touch, a potion, pure and divine. Two spirits merge, in love's eternal shine.",
        "In nature's embrace, love's whispers softly flow, Through rustling leaves and rivers' gentle glow. In her beauty, our hearts find solace and grow.",
        "In each task you aid, my admiration grows, Your diligence and grace, a love that flows. Beyond words, our bond, in every task it shows.",
        "In the rhythm of exercise, our passion ignites, Together we sweat, pushing limits, reaching new heights. In the gym's embrace, our bond strengthens, day and night.",
        "In hidden whispers, our love finds its spree, A clandestine dance, just you and me. In secrecy's embrace, our hearts roam free.",
        "In enchantment's spell, our hearts entwine, Magic's allure, a love divine. In mystical realms, our passions shine.",
        "Not even chatGPT could find something to say, about how this show can be used in a romantic way. ^_^"
    ]

      return (
        <>
        <div className="container-fluid" style={{ backgroundColor: "black" }}>
          <Row className="justify-content-around">
            {imageUrls.map((imageUrl, index) => (
              <Col key={index} xs={12} sm={6} md={4} lg={3} xl={3} style={{ marginBottom: "20px" }}>
                <Card style={{ width: "18rem", height: "100%" }}>
                  <Card.Img variant="top" src={imageUrl} style={{ width: "100%", height: "50%", objectFit: "cover" }} />
                  <Card.Body>
                    <Card.Title className="d-flex justify-content-center">{CardTitles[index]}</Card.Title>
                    <Card.Text>
                      {CardTexts[index]}
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </div>
        <div className="text-center text-info">
        <h1>The End</h1>
        </div>
        </>
      );
    }

    export default Act3_4;
