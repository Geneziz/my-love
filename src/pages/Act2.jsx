import React, { useState } from "react";
import SpeechComponent from "../components/Speech"; // adjust the path as needed
import Button from 'react-bootstrap/Button';
import Toasts from "../components/Toasts";

function Act2() {
  const [text, setText] = useState(
    `The second act, is comprised of facts,
    Here no fairytale lie
    and no fables told
    this is a matter of what i see
    a matter of what I behold`
  );

  const textt = `In hues of dawn, your portrait's drawn,
  A masterpiece, each line is dawned.
  Eyes that gleam with secrets spun,
  A smile that tells of tales begun.

  Brushstrokes dance in timeless grace,
  Capturing each fleeting trace.
  Your essence, like a gentle breeze,
  Whispers secrets through the trees.

  In every stroke, a story told,
  Of dreams and hopes, both young and old.
  A portrait not just of what you are,
  But of the journey, near and far.

  With each glance, a new detail found,
  In this portrait, love is crowned.
  For in the colors, I see true,
  The beauty that is only you.`;

  return (
    <>
      <div className="container-fluid" style={{ backgroundColor: "pink" }}>
        <div className="px-4 py-5 my-0 text-center">
          <h1 className="display-5 fw-bold text-white"> Act 2 </h1>

          <div className="col-lg-6 mx-auto">
            <Toasts />
            <img
              src="https://i.etsystatic.com/22530609/c/1769/1406/98/325/il/80261f/3570258910/il_340x270.3570258910_7z4k.jpg"
              alt="loving"
              style={{ maxWidth: "100%", cursor: "pointer" }}
            />

            <h2 className="text-white"> The Portrait of You </h2>


            <h3 className="lead mb-4" style={{ whiteSpace: "pre-line" }}>
              {textt}
            </h3>

            <SpeechComponent text={text} />
          </div>
        </div>

        <div className="text-center text-white">
        <Button variant="danger" href="/Act2_3">Explore Act</Button>{' '}
        </div>
      </div>
    </>
  );
}

export default Act2;
