import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function Door2() {
    const [text, setText] = useState({});
    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/Act1_2");
    };

    const getData = async () => {
        const response = await fetch(
            "https://poetrydb.org/title/spring/.json"
        );
        const data = await response.json();
        const randomIndex = Math.floor(Math.random() * data.length);
        setText(data[randomIndex]);
        console.log(data[randomIndex]);
        console.log(data);
    };

    const navbarStyle = {
        backgroundImage: `url('https://t3.ftcdn.net/jpg/05/72/71/84/360_F_572718478_dq7LlZrqa5QbZduwG9mGdHuHoNM5Glqd.jpg')`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="container-fluid" style={{ margin: 0, padding: 0, minHeight: '100vh', ...navbarStyle }} >
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card bg-dark text-white mt-5">
                        <div className="card-body">
                            <table className="table table-bordered table-responsive table-danger">
                                <tbody>
                                    <tr>
                                        <td className="fw-bold align-top text-center">
                                            {text.author}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="mb-4 text-center fw-bold">{text.title}</td>
                                    </tr>
                                    <tr>
                                        <td className="lead text-center">
                                            {text.lines && text.lines.join(" ")}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button
                                type="button"
                                className="btn btn-dark"
                                onClick={handleClick}
                            >
                                Return
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Door2;
