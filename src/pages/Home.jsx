import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import "../pages/Home.css";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [redirect, setRedirect] = useState(false);

  const text = `Prologue
  When the war of the beasts brings about the world's end
  The goddess descends from the sky
  Wings of light and dark spread afar
  She guides us to bliss, her gift everlasting

  Act I
  Infinite in mystery is the gift of the goddess
  We seek it thus, and take it to the sky
  Ripples form on the water's surface
  The wandering soul knows no rest

  Act II
  There is no hate, only joy
  For you are beloved by the goddess
  Hero of the dawn, Healer of worlds
  Dreams of the morrow hath the shattered soul
  Pride is lost
  Wings stripped away, the end is nigh

  Act III
  My friend, do you fly away now?
  To a world that abhors you and I?
  All that awaits you is a somber morrow
  No matter where the winds may blow
  My friend, your desire
  Is the bringer of life, the gift of the goddess
  Even if the morrow is barren of promises
  Nothing shall forestall my return...`;

  const navigate = useNavigate();

  useEffect(() => {
    const animationDuration = 57; // Seconds

    // Set a timeout to redirect after the animation duration
    const timeout = setTimeout(() => {
      setRedirect(true);
    }, animationDuration * 1000); // Convert seconds to milliseconds

    // Clean up the timeout when the component unmounts
    return () => clearTimeout(timeout);
  }, []);

  // Redirect to another page when redirect state becomes true
  if (redirect) {
    navigate("/Act1"); // Replace '/nextPage' with your desired destination
    // Alternatively, you can use React Router's <Navigate /> component if you're using React Router
  }

  return (
    <Container fluid style={{ backgroundColor: "black" }}>
      <div className="star-wars">
        <div className="crawl">
          <p className="title" style={{ whiteSpace: "pre-line" }}>
            {text}
          </p>
        </div>
      </div>
    </Container>
  );
};

export default Home;
