import React, { useState } from "react";
import Footer from "../components/Footer";
import SpeechComponent from "../components/Speech"; // adjust the path as needed
import Toasts from "../components/Toasts";

function Act1() {
  const [text, setText] = useState(
    `For the beginning act, my words are not enough.
    I must turn to savants of old, to convey my emotions,
    Perhaps they can tell of my devotion,
    with words as deep as the ocean,
    and as powerful as magic in a potion.
    Their wisdom woven into each verse,
    Unveiling a love that time can't disperse.`
  );

  const textt = `Your lips? I kiss that. Your body? I hug that.
  My smile? You cause that. Your heart? I don't deserve that.
  My heart? you own that. Your life? God blessed me with that,
  to love and to hold forever. My Aphrodite`;

  return (
    <>

      <div className="container-fluid" style={{ backgroundColor: "black" }}>

        <div className="px-4 py-5 my-0 text-center">
          <h1 className="display-5 fw-bold text-white"> Act 1 </h1>

          <div className="col-lg-6 mx-auto">
            <Toasts />
            <img
              src="https://media0.giphy.com/media/HSCZMUa1ao17h7l5mg/giphy.gif?cid=ecf05e47bfc6vl630g7jee077u6fkcq16v3szuridlufdtl3&ep=v1_gifs_related&rid=giphy.gif&ct=g"
              alt="loving"
              style={{ maxWidth: "100%", cursor: "pointer" }}
            />

            <h2 className="text-white"> Eternal Vow </h2>

            <p className="lead mb-4 text-white" style={{ whiteSpace: "pre-line" }}>
              {textt}
            </p>

            <SpeechComponent text={text} />
          </div>
        </div>

        <div className="text-center text-white">
          <Footer />
        </div>
      </div>
    </>
  );
}

export default Act1;
