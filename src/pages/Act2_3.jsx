import React, { useState } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Button from 'react-bootstrap/Button';
import "../pages/Act2_3.css";

function Act2_3() {
  const [activeKey, setActiveKey] = useState("home");
  const [spinning, setSpinning] = useState(false);

  const handleSelect = (key) => {
    if (key !== activeKey) {
      setSpinning(true);
      setTimeout(() => {
        setSpinning(false);
        setActiveKey(key);
      }, 500); // Duration of the spinning animation
    }
  };
  const text = `
  In flesh and bone, a vessel bound,
  A temple where the world is found.
  Muscles tense, and sinews tight,
  In every move, there's life's delight.

  Skin that whispers tales untold,
  Each scar a story, bold and old.
  Breath that dances, heartbeat's song,
  In every pulse, we do belong.

  But fleeting, fragile, this frame we wear,
  A cloak of dust, a mask of air.
  Yet in its confines, we learn to be,
  The embodiment of eternity`;

    const textt = `
    Deep within, where shadows sway,
    The soul resides, in night and day.
    A silent witness, a guiding light,
    In realms where darkness fears the night.

    Dreams unfurl, in secret flight,
    In the soul's embrace, they take their height.
    Emotions surge, a raging sea,
    In the soul's depths, we're wild and free.

    It's here, in whispers, the truth is heard,
    In the soul's language, without a word.
    It's here, in stillness, we find our peace,
    In the soul's solace, all conflicts cease.`;

    const texttt = `
    Beyond the body, beyond the soul,
    In realms untamed, the spirit's goal.
    A flame eternal, a cosmic spark,
    In the universe, it leaves its mark.

    It soars with eagles, dives with whales,
    In every breath, it sets its sails.
    It knows no bounds, no earthly ties,
    In spirit's realm, the infinite lies.

    In unity with all that is,
    The spirit dances, in pure bliss.
    It's the echo of the universe's song,
    In the spirit's embrace, we all belong.

    Each poem, a thread in life's grand weave,
    Body, soul, and spirit, they do cleave.
    Together, they form our being whole,
    In their union, we find our role.`;


  return (
    <>
      <div className="container-fluid" style={{ backgroundColor: "pink" }}>
        <div className="px-4 py-5 my-0 text-center">
        <img
              src="https://cdn.mos.cms.futurecdn.net/xRqbwS4odpkSQscn3jHECh-320-80.jpg"
              alt="loving"
              style={{ maxWidth: "100%", cursor: "pointer" }}
            />
          <Tabs
            activeKey={activeKey}
            onSelect={handleSelect}
            id="anim-tab-example"
            className="mb-3"
            fill
            transition={false} // Disable default transition
          >
            <Tab eventKey="body" title="Body">
              <div className={`tab-content ${spinning ? "spinning" : ""}`}
              style={{ backgroundColor: "lightyellow" }}>
                <h2>
                    BODY
                </h2>
                <div style={{ whiteSpace: "pre-line" }}>
                    {text}
                </div>
              </div>
            </Tab>
            <Tab eventKey="soul" title="Soul">
              <div className={`tab-content ${spinning ? "spinning" : ""}`}
              style={{ backgroundColor: "lightgreen" }}>
                <h2>
                    SOUL
                </h2>

              <div style={{ whiteSpace: "pre-line" }}>
                    {textt}
                </div>
               </div>
            </Tab>
            <Tab eventKey="spirit" title="Spirit">
            <div className={`tab-content ${spinning ? "spinning" : ""}`}
              style={{ backgroundColor: "lightblue" }}>
                <h2>
                    SPIRIT
                </h2>
              <div style={{ whiteSpace: "pre-line" }}>
                    {texttt}
                </div>
               </div>
            </Tab>
          </Tabs>
        </div>
        <div className="text-center text-white">
        <Button variant="danger" href="/Act3">To the Next Act</Button>{' '}
        </div>
      </div>
    </>
  );
}

export default Act2_3;
