import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './pages/Home';
import Act1 from "./pages/Act1";
import Act1_2 from "./pages/Act1_2";
import Door3 from "./pages/Door3";
import Door1 from "./pages/Door1";
import Door2 from "./pages/Door2";
import Act2 from "./pages/Act2";
import Act2_3 from "./pages/Act2_3";
import Act3 from "./pages/Act3";
import Act3_4 from "./pages/Act3_4";


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Act1" element={<Act1 />} />
        <Route path="/Act1_2" element={<Act1_2 />} />
        <Route path="/Door3" element={<Door3 />} />
        <Route path="/Door1" element={<Door1 />} />
        <Route path="/Door2" element={<Door2 />} />
        <Route path="/Act2" element={<Act2 />} />
        <Route path="/Act2_3" element={<Act2_3 />} />
        <Route path="/Act3" element={<Act3 />} />
        <Route path="/Act3_4" element={<Act3_4 />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
