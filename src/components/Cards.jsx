import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


function Cards() {
  return (
    <Container
  className="d-flex justify-content-center"
  style={{ maxWidth: '100%', height: '100vh', padding: "50px", backgroundColor: "black" }}
>
<Row className="justify-content-around">
    <Col xs={12} sm={6} md={4} lg={3} xl={3}> {/* Adjust column sizes as needed */}
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://t3.ftcdn.net/jpg/05/72/71/84/360_F_572718414_XoxnSXDzYHD0X1SA3zYIqo92C9ffyH6n.jpg" />
        <Card.Body>
          <Card.Title>Love</Card.Title>
          <Card.Text>
            Everytime you enter this door, you will read a different poem about love.
          </Card.Text>
          <Button variant="primary" href="/Door1">Enter</Button>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} sm={6} md={4} lg={3} xl={3}>
    <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://t3.ftcdn.net/jpg/05/72/71/84/360_F_572718478_dq7LlZrqa5QbZduwG9mGdHuHoNM5Glqd.jpg" />
        <Card.Body>
          <Card.Title>Spring</Card.Title>
          <Card.Text>
            This door holds poems of Spring, since you birth new life within me constantly .
          </Card.Text>
          <Button variant="primary" href="/Door2">Enter</Button>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} sm={6} md={4} lg={3} xl={3}>
    <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://t4.ftcdn.net/jpg/05/68/40/83/360_F_568408303_GS2gMDx8SWAGkEiZv8vVkbyle6IHCfQz.jpg" />
        <Card.Body>
          <Card.Title>Sonnets</Card.Title>
          <Card.Text>
            Here lies all of William Shakespeare's Sonnets. Who can say I love you like he can?
          </Card.Text>
          <Button variant="primary" href="/Door3">Enter</Button>
        </Card.Body>
      </Card>
    </Col>
    <div className="text-center text-white">
    <Button href="/Act2">To the Next Act</Button>{' '}
      </div>
  </Row>
</Container>
  );
}

export default Cards;
