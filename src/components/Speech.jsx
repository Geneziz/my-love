import React, { useEffect, useState } from "react";

function SpeechComponent({ text }) {
    const [voices, setVoices] = useState([]);

    useEffect(() => {
        const handleVoicesChanged = () => {
            const availableVoices = window.speechSynthesis.getVoices();
            setVoices(availableVoices);
            console.log(availableVoices);
        };

        // Add the event listener
        window.speechSynthesis.addEventListener('voiceschanged', handleVoicesChanged);

        // Call the handler immediately in case voices are already loaded
        handleVoicesChanged();

        // Clean up the event listener when the component unmounts
        return () => {
            window.speechSynthesis.removeEventListener('voiceschanged', handleVoicesChanged);
        };
    }, []);

    useEffect(() => {
        if (voices.length > 0) {
            const speech = new SpeechSynthesisUtterance(text);

            // Find a voice by its name
            const selectedVoice = voices.find(
                (voice) => voice.name === "Microsoft Mark - English (United States)"
            );

            if (selectedVoice) {
                // Set the voice for the speech synthesis
                speech.voice = selectedVoice;
            }

            console.log(speech);

            // Clean up by canceling the speech when the component unmounts
            return () => {
                window.speechSynthesis.cancel();
            };
        }
    }, [text, voices]);

    const handleClick = () => {
        const speech = new SpeechSynthesisUtterance(text);

        if (voices.length > 0) {
            const selectedVoice = voices.find(
                (voice) => voice.name === "Microsoft Mark - English (United States)"
            );
            if (selectedVoice) {
                speech.voice = selectedVoice;
            }
        }

        window.speechSynthesis.speak(speech);
    };

    return (
        <div>
            <button onClick={handleClick}>Speech</button>
        </div>
    );
}

export default SpeechComponent;
