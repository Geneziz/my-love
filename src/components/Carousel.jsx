import Carousel from 'react-bootstrap/Carousel';
import Container from 'react-bootstrap/Container';



function Carousels() {
  return (
    <Container
  className="d-flex justify-content-center align-items-center"
  style={{ maxWidth: '100%', height: '100vh', padding: 0, backgroundColor: "black" }}
>
  <Carousel
    style={{ maxHeight: '600px', width: '70%' }}
    className="d-inline-block"
  >
      <Carousel.Item>
      <img
          className="d-block mx-auto"
          src="https://img.freepik.com/premium-photo/beautiful-wallpaper-romantic-couple-near-red-heart_492154-4675.jpg"
          alt="First slide"
          style={{
            width: '100%', // Ensure the image spans the entire width
            maxHeight: '600px', // Optional: Set max height of the image
          }}
        />
        <Carousel.Caption>
          <h3>Fortress of Love</h3>
          <p>"In the closing scene, we retreat into our fortress, enveloped in the endeavor to love each other forever."</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <img
          className="d-block mx-auto"
          src="https://media1.tenor.com/m/SpDlTxKbFl8AAAAC/opmekk.gif"
          alt="Second slide"
          style={{
            width: '100%', // Ensure the image spans the entire width
            maxHeight: '600px', // Optional: Set max height of the image
            objectFit: 'cover',
          }}
        />
        <Carousel.Caption>
          <h3>Kiss me</h3>
          <p>Intoxicate me with your lips</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <img
          className="d-block mx-auto"
          src="https://media1.tenor.com/m/ZtMx62jdPoQAAAAC/dancing-in-the-moon-love.gif"
          alt="Third slide"
          style={{
            width: '100%', // Ensure the image spans the entire width
            maxHeight: '600px', // Optional: Set max height of the image
            objectFit: 'cover',
          }}
        />
        <Carousel.Caption className='text-danger'>
          <h3>Dance with me under the moon light</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
</Container>
  );
}

export default Carousels;
