import React, { useEffect, useState } from 'react';
import { Row, Col, Toast, Button } from 'react-bootstrap';

function Toasts() {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, []);

  return (
    <Row>
      <Col xs={6}>
        <Toast className="d-inline-block m-1" onClose={() => setShow(false)} show={show} delay={3000} autohide variant="primary">
          <Toast.Body>Make sure you click on Speech below!!!</Toast.Body>
        </Toast>
      </Col>
    </Row>
  );
}

export default Toasts;
